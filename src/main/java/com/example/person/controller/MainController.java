package com.example.person.controller;

import com.example.person.dto.Person;
import com.example.person.service.MainService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
@Slf4j
public class MainController {
   private final MainService mainService;

   @GetMapping("/get-all-person")
    public ResponseEntity<List<Person>> getAllPerson(){
       log.info("Выгрузился список");
       return ResponseEntity.ok(mainService.getAllPerson());
   }
}
