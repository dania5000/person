package com.example.person.service;

import com.example.person.dto.Person;

import java.util.List;

public interface MainService {
    List<Person> getAllPerson();
}
