package com.example.person.service.impl;

import com.example.person.dto.Gender;
import com.example.person.dto.Interests;
import com.example.person.dto.Person;
import com.example.person.service.MainService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class MainServiceImpl implements MainService {
    public List<Person> getAllPerson(){
        List<Person> personList = new ArrayList<>();
        personList.add(new Person("Петр", "Аалферов", 18, Gender.Male, false, Interests.КОШЕЧКИ));
        personList.add(new Person("Аифал", "Абабилов", 23, Gender.Male, true, Interests.МАШИНЫ));
        personList.add(new Person("Алексей", "Абабкова", 35, Gender.Male, false, Interests.ПРОГУЛКА));
        personList.add(new Person("Александр", "Абагов", 48, Gender.Male, true, Interests.ОБЩЕНИЕ));
        personList.add(new Person("Павел", "Абадаева", 15, Gender.Male, true, Interests.СЕМЬЯ));
        personList.add(new Person("Владимир", "Абадеев", 36, Gender.Male, false, Interests.ПРОГУЛКА));
        personList.add(new Person("Петр", "Абаденкова", 28, Gender.Male, true, Interests.СОБАЧКИ));
        personList.add(new Person("Константин", "Абадулин", 27, Gender.Male, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Игорь", "Абазьев", 28, Gender.Male, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Олег", "Абайдуллин", 39, Gender.Male, false, Interests.ПРОГУЛКА));
        personList.add(new Person("Антон", "Абайкина", 16, Gender.Male, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Данил", "Абакаев", 50, Gender.Male, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Николай", "Абакшина", 37, Gender.Male, true, Interests.МАШИНЫ));
        personList.add(new Person("Марк", "Абалаков", 45, Gender.Male, true, Interests.КОШЕЧКИ));
        personList.add(new Person("Константин", "Абалиева", 44, Gender.Male, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Руслан", "Абанин", 34, Gender.Male, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Владимир", "Абаршалина", 49, Gender.Male, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Евгений", "Абатуров", 24, Gender.Male, false, Interests.СЕМЬЯ));
        personList.add(new Person("Тимофей", "Иакашвили", 24, Gender.Male, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Денис", "Иаксимкин", 34, Gender.Male, true, Interests.МАШИНЫ));
        personList.add(new Person("Арсений", "Ианина", 27, Gender.Male, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Егор", "Иасафов", 28, Gender.Male, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Роман", "Ибадулина", 39, Gender.Male, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Никита", "Ибаев", 27, Gender.Male, false, Interests.ОБЩЕНИЕ));
        personList.add(new Person("Михаил", "Ибанес", 36, Gender.Male, true, Interests.КОШЕЧКИ));
        personList.add(new Person("Алексей", "Мааев", 25, Gender.Male, true, Interests.СОБАЧКИ));
        personList.add(new Person("Илья", "Ма-Си-Вен", 28, Gender.Male, true, Interests.МАШИНЫ));
        personList.add(new Person("Владимир", "Мависткулов", 29, Gender.Male, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Антон", "Мавлетов", 37, Gender.Male, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Владимир", "Мавлетшин", 21, Gender.Male, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Константин", "Мавлютов", 25, Gender.Male, false, Interests.СЕМЬЯ));
        personList.add(new Person("Ярослав", "Шааков", 26, Gender.Male, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Тимур", "Шабага", 27, Gender.Male, true, Interests.КОШЕЧКИ));
        personList.add(new Person("Олег", "Шабад", 38, Gender.Male, true, Interests.ОБЩЕНИЕ));
        personList.add(new Person("Максим", "Шабакаев", 39, Gender.Male, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Артём", "Обазим", 45, Gender.Male, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Егор", "Обакшина", 44, Gender.Male, false, Interests.МАШИНЫ));
        personList.add(new Person("Ярослав", "Обалин", 46, Gender.Male, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Акулина", "Казакова", 18, Gender.Female, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Арина", "Жукова", 17, Gender.Female, true, Interests.КОШЕЧКИ));
        personList.add(new Person("Боряна", "Журвалева", 14, Gender.Female, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Богдана", "Дубровская", 19, Gender.Female, true, Interests.СЕМЬЯ));
        personList.add(new Person("Берислава", "Дёмина", 24, Gender.Female, false, Interests.ОБЩЕНИЕ));
        personList.add(new Person("Берта", "Грачева", 34, Gender.Female, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Виола", "Гагарина", 32, Gender.Female, true, Interests.МАШИНЫ));
        personList.add(new Person("Виргиния", "Лаврентьева", 31, Gender.Female, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Виталина", "Некрасова", 30, Gender.Female, true, Interests.СОБАЧКИ));
        personList.add(new Person("Всеслава", "Перова", 38, Gender.Female, true, Interests.КОШЕЧКИ));
        personList.add(new Person("Владислава", "Преображенская", 52, Gender.Female, true, Interests.МАШИНЫ));
        personList.add(new Person("Владилена", "Патронова", 31, Gender.Female, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Диодора", "Сафронова", 25, Gender.Female, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Екатерина", "Толмачева", 32, Gender.Female, false, Interests.ПРОГУЛКА));
        personList.add(new Person("Жозефина", "Успенская", 42, Gender.Female, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Жизель", "Фирсова", 15, Gender.Female, true, Interests.КОШЕЧКИ));
        personList.add(new Person("Жанна", "Холодова", 22, Gender.Female, true, Interests.МАШИНЫ));
        personList.add(new Person("Заира", "Цветаева", 32, Gender.Female, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Захра", "Щербина", 23, Gender.Female, false, Interests.СЕМЬЯ));
        personList.add(new Person("Зульфия", "Эсце", 35, Gender.Female, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Злата", "Яковлева", 26, Gender.Female, true, Interests.ОБЩЕНИЕ));
        personList.add(new Person("Ипатия", "Ягужинская", 47, Gender.Female, true, Interests.МАШИНЫ));
        personList.add(new Person("Исидора", "Трифонова", 26, Gender.Female, true, Interests.КОШЕЧКИ));
        personList.add(new Person("Искра", "Тонева", 34, Gender.Female, true, Interests.ПРОГУЛКА));
        personList.add(new Person("Ираида", "Селезнева", 28, Gender.Female, true, Interests.СЕМЬЯ));
        personList.add(new Person("Инна", "Ржевская", 19, Gender.Female, true, Interests.МАШИНЫ));
        personList.add(new Person("Изольда", "Разина", 18, Gender.Female, true, Interests.КОШЕЧКИ));
        personList.add(new Person("Капитолина", "Черкасова", 15, Gender.Female, true, Interests.СОБАЧКИ));
        personList.add(new Person("Констанция", "Экель", 34, Gender.Female, true, Interests.ОБЩЕНИЕ));
        personList.add(new Person("Катиба", "Юдачева", 33, Gender.Female, true, Interests.МАШИНЫ));

        return personList;
    }

}
